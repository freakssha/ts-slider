import React from 'react';
import Slider from "./components/Slider/Slider";


const slides = [
    {
        img: 'https://cdn.theatlantic.com/thumbor/W544GIT4l3z8SG-FMUoaKpFLaxE=/0x131:2555x1568/1600x900/media/img/mt/2017/06/shutterstock_319985324/original.jpg',
        text: 'Кот в воздухе'
    },
    {
        img: 'https://www.americanhumane.org/app/uploads/2016/08/animals-cats-cute-45170-min.jpg',
        text: 'Маленькие коты'
    },
    {
        img: 'https://www.memphisveterinaryspecialists.com/files/best-breeds-of-house-cats-memphis-vet-1-1.jpeg',
        text: 'Глазастый кот'
    },
];

const App = () => {
    return (
        <Slider
            slides={slides}
            loop={true}
            navs={true}
            pags={true}
            auto={true}
            delay={3}
            stopMouseHover={true}
        />
    );
}

export default App;
