import {ISlide} from "../../components/Slider/Slider";
import {PaginationDot, PaginationDotsContainer} from "./styles";
import React from "react";

interface PaginationDotsProps {
    slides: ISlide[],
    slideId: number,
    goToSlideWithID: (e: React.MouseEvent<HTMLElement>) => void,
}

const PaginationDots = ({slides, slideId, goToSlideWithID}: PaginationDotsProps) => {
    return (
        <PaginationDotsContainer>
            {
                slides.map((slide: ISlide, id: number) =>
                    <PaginationDot
                        key={id}
                        id={id}
                        onClick={goToSlideWithID}
                        style={{backgroundColor: `${(slideId === id) ? '#8B8B8B' : '#bbb'}`}}
                    />
                )
            }
        </PaginationDotsContainer>
    );
}

export default PaginationDots;