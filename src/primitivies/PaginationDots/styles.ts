import styled from "styled-components";

export const PaginationDotsContainer: any = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 5px;
`

export const PaginationDot: any = styled.span`
  height: 14px;
  width: 14px;
  border-radius: 50%;
  display: inline-block;
  margin: 5px;
  cursor: pointer;
`
