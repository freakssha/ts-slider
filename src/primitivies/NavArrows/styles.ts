import styled from "styled-components";

export const NavArrow: any = styled.a`
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: white;
  font-weight: bold;
  font-size: 16px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  right: ${(props: any) => props.right ? 0 : 100};
`
