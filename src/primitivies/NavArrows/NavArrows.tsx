import React from 'react';
import {NavArrow} from "./styles";


interface INavArrowsProps {
    previousSlide: () => void,
    nextSlide: () => void,
}


const NavArrows = ({previousSlide, nextSlide}: INavArrowsProps) => {
    return (
        <div>
            <NavArrow onClick={previousSlide}>&#10094;</NavArrow>
            <NavArrow right onClick={nextSlide}>&#10095;</NavArrow>
        </div>
    );
}

export default React.memo(NavArrows);