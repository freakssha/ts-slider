import {ISlide, SliderFunctions} from "../components/Slider/Slider";

interface IAction {
    type: string,
    payload: any,
}

export default function slideIdReducer(state: ISlide[], action: IAction) {
    switch (action.type) {
        case SliderFunctions.NEXT:
            if (action.payload.loop) {
                return (action.payload.slideId + 1 === action.payload.slidesLength) ? 0 : (action.payload.slideId + 1)
            } else {
                return (action.payload.slideId + 1 === action.payload.slides.length) ? action.payload.slideId : (action.payload.slideId + 1)
            }
        case SliderFunctions.PREVIOUS:
            if (action.payload.loop) {
                return (action.payload.slideId === 0) ? (action.payload.slidesLength - 1) : (action.payload.slideId - 1)
            } else {
                return (action.payload.slideId === 0) ? action.payload.slideId : (action.payload.slideId - 1)
            }
        case SliderFunctions.GO_TO_SLIDE_WITH_ID:
            return +action.payload
        default:
            return state
    }
}