import React, {useCallback, useEffect, useReducer, useState} from 'react';
import {Slide, SlideImage, SlideName, SlideNumber, SliderContainer} from "./styles";
import slideIdReducer from "../../reducers/slideIdReducer";
import NavArrows from "../../primitivies/NavArrows/NavArrows";
import PaginationDots from "../../primitivies/PaginationDots/PaginationDots";
import {DEFAULT_DELAY, DELAY_TO_MS_RATIO} from "../../constants";

export interface ISlide {
    img: string,
    text: string,
}

interface ISliderProps {
    slides: ISlide[],
    loop?: boolean,
    navs?: boolean,
    pags?: boolean,
    auto?: boolean,
    stopMouseHover?: boolean,
    delay?: number,
}

export enum SliderFunctions {
    NEXT = 'next',
    PREVIOUS = 'previous',
    GO_TO_SLIDE_WITH_ID = 'goToSlideWithID',
}

const Slider = ({
                    slides,
                    loop = true,
                    navs = true,
                    pags = true,
                    auto = false,
                    stopMouseHover = false,
                    delay = 5
                }: ISliderProps) => {
    const [slideId, setSlideId]: any = useReducer(slideIdReducer, 0)
    const [isHover, setIsHover]: any = useState(false)

    const onMouseHover = useCallback((): void => {
        if (stopMouseHover) {
            setIsHover(!isHover)
        }
    }, [isHover, setIsHover])

    useEffect(() => {
        if (auto && (stopMouseHover && !isHover)) {
            const autoDelay = setTimeout(() => setSlideId({
                    type: SliderFunctions.NEXT,
                    payload: {loop, slideId, slidesLength: slides.length}
                }), delay * DELAY_TO_MS_RATIO || DEFAULT_DELAY)
            return () => clearTimeout(autoDelay)
        }
    }, [isHover, setIsHover, slideId, setSlideId]);

    return (
        <SliderContainer>
            <Slide onMouseOver={onMouseHover} onMouseOut={onMouseHover}>
                <SlideNumber>{`${slideId + 1} / ${slides.length}`}</SlideNumber>
                <SlideName>{slides[slideId].text}</SlideName>
                <SlideImage src={slides[slideId].img}/>
            </Slide>

            {
                navs ? (
                    <NavArrows previousSlide={() =>
                        setSlideId({
                            type: SliderFunctions.PREVIOUS,
                            payload: {loop, slideId, slidesLength: slides.length}
                        })}
                               nextSlide={() => setSlideId({
                                   type: SliderFunctions.NEXT,
                                   payload: {loop, slideId, slidesLength: slides.length}
                               })}
                    />
                ) : ''
            }
            {
                pags ? (
                    <PaginationDots slides={slides}
                                    slideId={slideId}
                                    goToSlideWithID={(e: React.MouseEvent<HTMLElement>) =>
                                        setSlideId({
                                            type: SliderFunctions.GO_TO_SLIDE_WITH_ID,
                                            payload: e.currentTarget.id
                                        })}
                    />
                ) : ''
            }
        </SliderContainer>
    );
}

export default Slider;