import styled from "styled-components";

export const SliderContainer: any = styled.div`
  max-width: 90%;
  position: relative;
  margin: 10%;
  height: 300px;
`

export const Slide: any = styled.div`
  color: #fff;
`

export const SlideNumber: any = styled.p`
  position: absolute;
  margin: 0.5rem;
`

export const SlideName: any = styled.p`
  position: absolute;
  bottom: 0;
  left: 45%;
`

export const SlideImage: any = styled.img`
  object-fit: cover;
  width: 100%;
  height: 300px;
`

